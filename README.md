# Revolut Currency Converter

App built under MVVM architecture. Using LiveData for Data Binding, Retrofit and okhttp for network connection, RxJava for rates polling service, Room as persistance library and Kotlin.