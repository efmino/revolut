package com.example.revolut.utils

import junit.framework.Assert.assertNull
import org.junit.Test

class UtilsUnitTest {

    @Test
    fun testStringIsNumeric() {
        val one = "123"
        val two = "12.3"
        val three = "12..3"
        val four = ".."
        val five = ""

        assert(stringIsNumeric(one))
        assert(stringIsNumeric(two))
        assert(!stringIsNumeric(three))
        assert(!stringIsNumeric(four))
        assert(!stringIsNumeric(five))
    }

    @Test
    fun testStringToFloat() {
        val one = "123"
        val two = "12.3"
        val three = "12..3"
        val four = ".."
        val five = ""
        val six = "0"

        val testOne = stringToFloat(one)
        checkNotNull(testOne)
        assert(testOne.equals(123.toFloat()))
        val testTwo = stringToFloat(two)
        checkNotNull(testTwo)
        assert(testTwo.equals(12.3.toFloat()))
        val testThree = stringToFloat(three)
        assertNull(testThree)
        val testFour = stringToFloat(four)
        assertNull(testFour)
        val testFive = stringToFloat(five)
        assertNull(testFive)
        val testSix = stringToFloat(six)
        checkNotNull(testSix)
        assert(testSix.equals(0.toFloat()))
    }

    @Test
    fun testParseAmount() {
        val one = 123.toFloat()
        val two = (12.3).toFloat()
        val three = 0.toFloat()
        val four = 0.001.toFloat()
        val five = (12.335677).toFloat()

        assert(parseAmount(one).equals("123"))
        assert(parseAmount(two).equals("12.30"))
        assert(parseAmount(three).equals("0"))
        assert(parseAmount(four).equals("0"))
        assert(parseAmount(five).equals("12.34"))
    }
}