package com.example.revolut

import android.content.Context
import androidx.room.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.IOException
import androidx.test.core.app.ApplicationProvider
import com.example.revolut.database.*
import junit.framework.TestCase.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class Database {
    private lateinit var rateDatabaseDao: RateDatabaseDao
    private lateinit var currencyDatabaseDao: CurrencyDatabaseDao
    private lateinit var db: RevolutDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, RevolutDatabase::class.java
        ).build()

        rateDatabaseDao = db.rateDatabaseDao
        currencyDatabaseDao = db.currencyDatabaseDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun ratesInsertAndReadInDb() {
        val rate1 = Rate("ABC", 0.toFloat(), "USD", "123")
        val rate2 = Rate("DEF", 0.toFloat(), "USD", "123")
        val rate3 = Rate("GHI", 0.toFloat(), "USD", "123")

        rateDatabaseDao.insertAll(arrayOf(rate1, rate2, rate3))

        // Test get rates
        val liveRates = rateDatabaseDao.getRates()
        assertEquals(liveRates.size, 3)
    }

    @Test
    fun currenciesInsertAndReadInDb() {
        val rate2SelectedAt = System.currentTimeMillis()+3
        val rate1 = Currency("ABC", "abc", System.currentTimeMillis())
        val rate2 = Currency("DEF", "def", rate2SelectedAt)
        val rate3 = Currency("GHI", "ghi", System.currentTimeMillis()+1)

        currencyDatabaseDao.insertAll(arrayOf(rate1, rate2, rate3))

        // Test get currencies
        val currencies = currencyDatabaseDao.getCurrencies()
        assertNotNull(currencies)
        assertEquals(currencies.size, 3)

        // Test selected currency
        val selectedCurrency = currencyDatabaseDao.getSelectedCurrency()
        assertEquals(selectedCurrency.symbol, rate2.symbol)

        // Test update
        selectedCurrency.selectedAt = System.currentTimeMillis() + 1
        currencyDatabaseDao.update(selectedCurrency)

        // Test update
        val selectedCurrency2 = currencyDatabaseDao.getSelectedCurrency()
        assertEquals(selectedCurrency2.symbol, rate2.symbol)
        assertNotSame(selectedCurrency2.selectedAt, rate2SelectedAt)
    }
}
