//
// Created by Esteban Mino on 2019-11-03.
//

#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring

JNICALL
Java_com_example_revolut_Keys_fixerApiKey(JNIEnv *env, jobject object) {
    std::string fixer_api_key = "9ad5e710d01cb3d9a25ace4a2d0ea30c";
    return env->NewStringUTF(fixer_api_key.c_str());
}