package com.example.revolut.repository

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.revolut.Keys
import com.example.revolut.database.*
import com.example.revolut.network.Network
import com.example.revolut.network.asDatabaseModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RatesRepository (revolutDatabase: RevolutDatabase, private val context: Context) {

    private val currencyDatabaseDao: CurrencyDatabaseDao = revolutDatabase.currencyDatabaseDao
    private val rateDatabaseDao: RateDatabaseDao = revolutDatabase.rateDatabaseDao

    suspend fun refreshRates(base: String) {
        withContext(Dispatchers.IO) {
            launch {
                Network.revolut(context).getObservableRates(base).subscribe({ rates ->
                    rateDatabaseDao.insertAll(rates.asDatabaseModel())
                }, {error -> Log.i("Respository error: ", error.message)})
            }
        }
    }

    suspend fun init() {
        withContext(Dispatchers.IO) {
            Network.revolut(context).getObservableInitialRates().subscribe({ rates ->
                rateDatabaseDao.insertAll(rates.asDatabaseModel())
            }, {error -> Log.i("Respository error: ", error.message)})
            val rates = rateDatabaseDao.getRates()
            val rateSymbols = rates.map {
                it.symbol
            }
            Network.fixer(context).getCurrencies(Keys.fixerApiKey()).subscribe({currencies ->
                currencyDatabaseDao.insertAll(currencies.asDatabaseModel(rateSymbols))
            }, {error -> Log.i("Respository error: ", error.message)})
        }
    }

    fun getAllRates(): LiveData<List<Rate>> {
        return rateDatabaseDao.getLiveRates()
    }

    fun getAllCurrencies(): LiveData<List<Currency>> {
        return currencyDatabaseDao.getLiveCurrencies()
    }

    suspend fun updateCurrencySelectedAt(currency: Currency) {
        withContext(Dispatchers.IO) {
            currency.selectedAt = System.currentTimeMillis()
            currencyDatabaseDao.update(currency)
        }
    }


}
