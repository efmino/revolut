package com.example.revolut

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.revolut.service.PollingService
import android.content.Intent
import android.net.ConnectivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.IntentFilter
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val intent = Intent(this, PollingService::class.java)
        startService(intent)
    }

    @Suppress("DEPRECATION")
    override fun onResume() {
        super.onResume()
        registerReceiver(networkStateReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(networkStateReceiver);
    }

    override fun onDestroy() {
        super.onDestroy()
        val intent = Intent(this, PollingService::class.java)
        stopService(intent)
    }

    private val networkStateReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val ni = manager.activeNetworkInfo
            if (ni === null ) {
                Toast.makeText(applicationContext, R.string.no_internet_connection,
                    Toast.LENGTH_LONG).show()
            }
        }
    }
}
