package com.example.revolut

import android.text.TextWatcher
import android.widget.EditText
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.revolut.rates.CurrencyRateListener
import android.text.Editable
import com.example.revolut.rates.RatesViewModel
import android.view.inputmethod.InputMethodManager
import android.app.Activity
import com.example.revolut.database.Currency
import com.example.revolut.utils.stringToFloat


fun imageTransform (symbol: String): String {
    return "https://raw.githubusercontent.com/transferwise/currency-flags/master/src/flags/${symbol.toLowerCase()}.png"
}

@BindingAdapter("symbol")
fun bindImage(imgView: ImageView, symbol: String?) {
    symbol?.let {
        val imgUri = imageTransform(symbol).toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
            .load(imgUri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation))
                    .error(R.drawable.ic_broken_image)
            .into(imgView)
    }
}


@BindingAdapter(value = ["currencyRateListener", "currency", "ratesViewModel"])
fun amountInputEditText(editText: EditText, currencyRateListener: CurrencyRateListener, currency: Currency,
                        ratesViewModel: RatesViewModel?) {

    var localHasFocus = false
    val imm = editText.context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    editText.setOnFocusChangeListener { view, hasFocus ->
        localHasFocus = hasFocus
        if (hasFocus) {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
            currencyRateListener.onClick(currency, editText)

        } else {
            imm.hideSoftInputFromWindow(view.windowToken, 0)
            currencyRateListener.onUnFocus(editText)
        }
    }
//
    editText.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            ratesViewModel?.let {
                if (localHasFocus) {
                    ratesViewModel.onAmountToConvertChange(stringToFloat(s.toString()))
                }
            }

        }
    })
}