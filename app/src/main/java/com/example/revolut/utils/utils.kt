package com.example.revolut.utils

import java.lang.Float.parseFloat
import java.math.RoundingMode

/**
 * Check if a string can be formatted as number
 *
 * @return Boolean - Whether the string is in number format
 */
fun stringIsNumeric(string: String): Boolean {
    try {
        parseFloat(string)
    } catch (e: Throwable) {
        return false
    }
    return true
}

/**
 * Generates a float from a numeric string
 * Returns 0 if it fails
 *
 * @return Float - Float format of the string
 */
fun stringToFloat(string: String): Float? {
    return try {
        parseFloat(string)
    } catch (e: Throwable) {
        null
    }
}

/**
 * Parse a float into a more convenient way to display numbers
 *
 * @return String - Formatted string
 */
fun parseAmount(float: Float): String {
    return try {
        val d = float.toBigDecimal().setScale(2, RoundingMode.HALF_UP)
        val int = d.toBigInteger()
        if (d.minus(int.toBigDecimal()).compareTo(0.01.toBigDecimal()) == -1) {
            int.toString()
        } else {
            "%.2f".format(float)
        }
    } catch (e: Throwable) {
        ""
    }
}