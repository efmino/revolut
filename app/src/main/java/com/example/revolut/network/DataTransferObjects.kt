package com.example.revolut.network

import com.example.revolut.database.Currency
import com.example.revolut.database.Rate
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NetworkRateContainer(val base: String, val rates: Map<String, Float>)

@JsonClass(generateAdapter = true)
data class NetworkCurrencyContainer(val symbols: Map<String, String>)

fun NetworkRateContainer.asDatabaseModel(): Array<Rate> {

    val list = rates.map {
        Rate (
            symbol = it.key,
            rate = it.value,
            base = base,
            timestamp = System.currentTimeMillis().toString()
        )
    }.toMutableList()
    list.add((
        Rate (
            symbol = base,
            rate = 1.toFloat(),
            base = base,
            timestamp = System.currentTimeMillis().toString()
        )
    ))
    return list.toTypedArray()
}

fun NetworkCurrencyContainer.asDatabaseModel(rates: List<String>): Array<Currency> {
    val symbolsToAdd = symbols.filter {
        rates.contains(it.key)
    }
    var i = 0
    return symbolsToAdd.map {
        i += 1
        Currency (
            symbol = it.key,
            name = it.value,
            selectedAt = System.currentTimeMillis() + i
        )
    }.toTypedArray()
}
