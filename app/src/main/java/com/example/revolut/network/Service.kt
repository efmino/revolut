package com.example.revolut.network

import android.content.Context
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit


val REVOLUT_BASE_URL = "https://revolut.duckdns.org/"
val FIXER_BASE_URL = "http://data.fixer.io/api/"

interface RevolutService {
    @GET("/latest")
    fun getObservableRates(@Query("base") base: String): Observable<NetworkRateContainer>

    @GET("/latest?base=EUR")
    fun getObservableInitialRates(): Observable<NetworkRateContainer>
}

interface FixerService {
    @GET("/symbols")
    fun getCurrencies(@Query("access_key") API_KEY: String): Observable<NetworkCurrencyContainer>
}

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

object Network {
    private fun revolutRetrofit(context: Context) = Retrofit.Builder()
        .baseUrl(REVOLUT_BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpBuilder(context))
        .build()

    private fun fixerRetrofit(context: Context) = Retrofit.Builder()
        .baseUrl(FIXER_BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .client(okHttpBuilder(context))
        .build()

    private fun okHttpBuilder(context: Context) = OkHttpClient.Builder()
        .addInterceptor(NetworkConnectionInterceptor(context))
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .build()

    fun fixer(context: Context): FixerService = fixerRetrofit(context).create(FixerService::class.java)
    fun revolut(context: Context): RevolutService = revolutRetrofit(context).create(RevolutService::class.java)
}

