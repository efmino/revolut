package com.example.revolut.network

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import android.net.ConnectivityManager


class NetworkConnectionInterceptor(private val context: Context) : Interceptor {

    private val isConnected: Boolean
        get() {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = connectivityManager.activeNetworkInfo
            return netInfo != null && netInfo.isConnected
        }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isConnected) {
            throw NoConnectivityException()
        }

        val builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

}

class NoConnectivityException : IOException() {
    override val message: String?
        get() = "No Internet Connection"
}
