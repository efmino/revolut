package com.example.revolut

object Keys {

    init {
        System.loadLibrary("native-lib")
    }

    external fun fixerApiKey(): String
}