package com.example.revolut.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CurrencyDatabaseDao {

    @Update
    fun update(currency: Currency)

    @Query("SELECT * FROM currency_table ORDER BY selectedAt DESC")
    fun getLiveCurrencies(): LiveData<List<Currency>>

    @Query("SELECT * FROM currency_table ORDER BY selectedAt DESC")
    fun getCurrencies(): List<Currency>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(currencies: Array<Currency>)

    @Query("SELECT * FROM currency_table ORDER BY selectedAt DESC LIMIT 1")
    fun getSelectedCurrency(): Currency

}
