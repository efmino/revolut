package com.example.revolut.database

import androidx.room.*

@Entity(tableName = "currency_table")
data class Currency(
    @PrimaryKey
    @ColumnInfo(name = "symbol")
    val symbol: String,

    @ColumnInfo(name = "name")
    var name: String,

    @ColumnInfo(name = "selectedAt")
    var selectedAt: Long
)
