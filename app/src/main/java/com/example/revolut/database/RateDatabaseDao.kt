package com.example.revolut.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface RateDatabaseDao {

    @Query("SELECT * FROM rates_table")
    fun getLiveRates(): LiveData<List<Rate>>

    @Query("SELECT * FROM rates_table")
    fun getRates(): List<Rate>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(rates: Array<Rate>)

}

