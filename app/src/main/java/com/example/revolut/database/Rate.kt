package com.example.revolut.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "rates_table")
data class Rate(

    @PrimaryKey
    val symbol: String,

    @ColumnInfo(name = "rate")
    var rate: Float,

    @ColumnInfo(name = "base")
    var base: String,

    @ColumnInfo(name = "timestamp")
    var timestamp: String

)