package com.example.revolut.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [Currency::class, Rate::class], version = 1, exportSchema = false)
abstract class RevolutDatabase : RoomDatabase() {

    abstract val rateDatabaseDao: RateDatabaseDao
    abstract val currencyDatabaseDao: CurrencyDatabaseDao

    companion object {

        @Volatile
        private var INSTANCE: RevolutDatabase? = null

        fun getInstance(context: Context): RevolutDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        RevolutDatabase::class.java,
                        "revolut_database"
                    ).build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}
