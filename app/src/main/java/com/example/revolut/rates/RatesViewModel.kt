package com.example.revolut.rates

import android.app.Application
import androidx.lifecycle.*
import com.example.revolut.database.*
import com.example.revolut.database.Currency
import com.example.revolut.repository.RatesRepository
import kotlinx.coroutines.*
import androidx.lifecycle.LiveData

class RatesViewModel(
        application: Application) : AndroidViewModel(application) {

    private val viewModelJob = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _amountToConvert = MutableLiveData<Float>()
    val amountToConvert: LiveData<Float>
            get() = _amountToConvert

    var convertedAmount = ""

    private val justSelectedCurrency = MutableLiveData<Boolean>()

    private val database = RevolutDatabase.getInstance(application)
    private val ratesRepository = RatesRepository(database, application)

    val currencies = ratesRepository.getAllCurrencies()
    val rates = ratesRepository.getAllRates()

    val ratesToObserve = ratesLivedata()

    private fun ratesLivedata(): LiveData<List<Rate>> {
        return MediatorLiveData<List<Rate>>().apply {
            var lastAmountToConvert: Float? = null
            var lastRates: List<Rate>? = null
            var lastCurrencySelected: Currency? = null

            fun updateAmountToConvert() {
                this.value = lastRates
            }

            addSource(rates) {
                it?.let{
                    lastRates = it
                }
            }
            addSource(amountToConvert) {
                if (it != lastAmountToConvert) {
                    updateAmountToConvert()
                    lastAmountToConvert = it
                }
            }
            addSource(currencies) {
                it?.let {
                    if (it[0].symbol != lastCurrencySelected?.symbol) {
                        updateAmountToConvert()
                        lastCurrencySelected = it[0]
                    }
                }
            }
            addSource(justSelectedCurrency) {
                if (it) {
                    updateAmountToConvert()
                }
            }
        }
    }

    init {
        init()
    }

    private fun init() {
        uiScope.launch {
            ratesRepository.init()
        }
    }

    fun onSelectedCurrency() {
        justSelectedCurrency.value = true
        justSelectedCurrency.value = false
    }

    fun onConvertedAmountChange(amount: String) {
        convertedAmount = amount
    }

    fun onAmountToConvertChange(amountToConvert: Float?) {
        _amountToConvert.value = amountToConvert
    }

    fun onCurrencyRateClicked(currency: Currency) {
        uiScope.launch {
            ratesRepository.refreshRates(currency.symbol)
            ratesRepository.updateCurrencySelectedAt(currency)
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}