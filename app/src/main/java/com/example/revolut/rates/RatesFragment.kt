package com.example.revolut.rates


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.revolut.R
import com.example.revolut.databinding.FragmentRatesBinding

class RatesFragment : Fragment() {

    lateinit var adapter: RateAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        val binding: FragmentRatesBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_rates, container, false
        )

        val application = requireNotNull(this.activity).application

        val ratesViewModelFactory = RatesViewModelFactory(application)

        val ratesViewModel = ViewModelProviders.of(this, ratesViewModelFactory).get(RatesViewModel::class.java)
        val clickListener = CurrencyRateListener( {
                currency, editText ->
                    val tempHint = editText.hint.toString()
                    ratesViewModel.onConvertedAmountChange(tempHint)
                    ratesViewModel.onCurrencyRateClicked(currency)
                    editText.requestFocus()
        }, {
                editText ->
                    if(editText.text.isNotEmpty()) editText.text.clear()
        })

        adapter = RateAdapter(this, clickListener, ratesViewModel)

        binding.ratesRecyclerView.adapter = adapter
        binding.lifecycleOwner = this

        ratesViewModel.currencies.observe(this, Observer {
            adapter.submitList(it)
            if (it.isNotEmpty()) adapter.selectedCurrency.value = it[0]
        })
        return binding.root
    }
}

