package com.example.revolut.rates

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class RatesViewModelFactory (
    private val application: Application) : ViewModelProvider.Factory {

        @Suppress("unchecked_cast")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(RatesViewModel::class.java)) {
                return RatesViewModel(application) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
}