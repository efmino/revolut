package com.example.revolut.rates

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.revolut.database.Currency
import com.example.revolut.databinding.ListItemRateBinding
import com.example.revolut.utils.stringIsNumeric
import com.example.revolut.utils.stringToFloat

class RateAdapter(private val fragment: Fragment,
                  private val clickListener: CurrencyRateListener,
                  private val ratesViewModel: RatesViewModel
) :
    ListAdapter<Currency, RateAdapter.ViewHolder>(CurrencyRateDiffCallback()) {

    val selectedCurrency = MutableLiveData<Currency?>()


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(fragment, item, ratesViewModel, clickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ListItemRateBinding.inflate(layoutInflater, parent, false)
        val viewHolder = ViewHolder(binding)
        binding.lifecycleOwner = viewHolder
        return viewHolder
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.binding.amountInputEditText.text.clear()
    }

    override fun onViewAttachedToWindow(holder: ViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.markAttach()
    }

    inner class ViewHolder constructor(val binding: ListItemRateBinding): RecyclerView.ViewHolder(binding.root), LifecycleOwner {

        private val lifecycleRegistry = LifecycleRegistry(this)

        init {
            lifecycleRegistry.currentState = Lifecycle.State.INITIALIZED
            ratesViewModel.onSelectedCurrency()
        }

        fun markAttach() {
            lifecycleRegistry.currentState = Lifecycle.State.STARTED

        }

        override fun getLifecycle(): Lifecycle {
            return lifecycleRegistry
        }

        var currencySelected: Currency? = null
        var rate: Float? = null

        fun bind(fragment: Fragment, currency: Currency, ratesViewModel: RatesViewModel,
                 currencyRateListener: CurrencyRateListener) {
            lifecycleRegistry.currentState = Lifecycle.State.CREATED
            binding.currency = currency
            binding.currencyRateListener = currencyRateListener
            binding.ratesViewModel = ratesViewModel

            // If is the selected currency
            selectedCurrency.observe(this, Observer {
                it?.let {
                    currencySelected = it
                    if (currency.symbol == it.symbol) {
                        val realHint = ratesViewModel.convertedAmount
                        if(stringIsNumeric(realHint) && stringToFloat(realHint) != 0.toFloat()) {
                            binding.amountInputEditText.setText(realHint)
                            binding.amountInputEditText.setSelection(realHint.length)
                            ratesViewModel.onSelectedCurrency()
                        }
                    } else {
                        binding.amountInputEditText.text.clear()
                    }
                }
            })

            // Get rate for binding
            ratesViewModel.ratesToObserve.observe(fragment, Observer {rates ->
                currencySelected?.let {currencySelected ->
                    rates?.find {
                            rate -> rate.symbol == currency.symbol && rate.base == currencySelected.symbol
                    }?.let {
                        binding.rate = it.rate
                    }
                }
            })

            ratesViewModel.onSelectedCurrency()

            currencySelected?.let {
                if (it.symbol == currency.symbol && ratesViewModel.amountToConvert.value != null) {
                    binding.amountInputEditText.setText(ratesViewModel.amountToConvert.value.toString())
                }
            }

            binding.executePendingBindings()
        }

    }
}

class CurrencyRateDiffCallback: DiffUtil.ItemCallback<Currency>() {

    override fun areItemsTheSame(oldItem: Currency, newItem: Currency): Boolean {
        return oldItem.symbol == newItem.symbol
    }

    override fun areContentsTheSame(oldItem: Currency, newItem: Currency): Boolean {
        return oldItem == newItem
    }
}

class CurrencyRateListener(val clickListener: (currency: Currency, editText: EditText) -> Unit,
                           val unFocusListener: (editText: EditText) -> Unit) {

    var lastCurrencySelected: Currency? = null

    fun onClick(currency: Currency, editText: EditText) {
        if (lastCurrencySelected == null || lastCurrencySelected?.symbol != currency.symbol ) {
            lastCurrencySelected = currency
            clickListener(currency, editText)
        }
    }

    fun onUnFocus(editText: EditText) = unFocusListener(editText)

}