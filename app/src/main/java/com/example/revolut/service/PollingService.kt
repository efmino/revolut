package com.example.revolut.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import androidx.annotation.Nullable
import com.example.revolut.database.RevolutDatabase
import com.example.revolut.network.Network
import com.example.revolut.network.asDatabaseModel
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class PollingService : Service() {
    var observable: Disposable? = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        observable = Observable.interval(1, TimeUnit.SECONDS, Schedulers.io())
            .observeOn(Schedulers.newThread())
            .map {
                val currencySelected = RevolutDatabase.getInstance(application).currencyDatabaseDao.getSelectedCurrency()
                Network.revolut(this).getObservableRates(currencySelected.symbol)
            }
            .doOnError { error -> Log.i("Polling error: ", error.toString()) }
            .retry()
            .subscribe( { s -> s?.subscribe( {
                it?.let {
                    RevolutDatabase.getInstance(application).rateDatabaseDao.insertAll(it.asDatabaseModel())
                }
            }, {onError -> Log.i("Polling sub error: ", onError.toString())})},
                {onError -> Log.i("Polling sub error: ", onError.toString())})

        return START_STICKY
    }

    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onDestroy() {
        observable?.dispose()
        stopSelf()
        super.onDestroy()
    }

}

